<?php

include('connexion.php');

// initialize empty array
$insert_reqs = [];

// users
$insert_reqs['users'] = "INSERT INTO users (name,lastname,email,password,adresse,phone,activited,city_id,inscription_date)VALUES
('latifa','lhafidi','latifa4smi@gmail.com','latifa2022','73rue de peovence','0763458795',1,4,'22-12-03'),
('oliv','spai','olivspail@gmail.com','oliv2022','12rue de peovence','0754891231',1,3,'22-11-13'),
('benaventure','kj','bena@gmail.com','123456','73rue de peovence','0765628798',2,6,'22-02-03')";

// product_categories
$insert_reqs['product_categories'] = "INSERT INTO product_categories (category)VALUES('ordinateur'),('ecran'),('souris'),('clavier')";

// opinions
$insert_reqs['opinions'] = "INSERT INTO opinions (user_id,product_id,star_count,date,opinion) 
VALUES
(1,1, 4,'2020-03-02','Bon produit prix abordable bon emballage livraison rapide Autonomie un peu juste sinon supetUsage professionnel'),
(2,1, 3,'2021-09-08','Bon portable Lesavantages disque dur SSD memoire 8Go léger'),
(3,1, 2,'2022-11-05','pas satisfait la batterie pas accessiblepas beaucoup de ports USB')";

// cities
$insert_reqs['cities'] = "INSERT INTO cities (city,postal_code,country)VALUES('Dunkerque',59140,'france'),('Grande synthe',59760,'france'),('Saint pol',59430,'france')";

// orders
$insert_reqs['orders'] = "INSERT INTO orders (user_id,total,means_of_payment,delivery_method,products_count,delivery_date) VALUES
(1,1002,'carte bancaire','poste',2,'2022-01-13'),(2,968.96,'chèque','relais',3,'2022-01-13')";

// cart
$insert_reqs['cart'] = "INSERT INTO cart(total_cart,product_count,state_cart,order_id)VALUES
(985,2,1,3),(1568.2,3,1,2)";

// ordered_products
$insert_reqs['ordered_products'] = "INSERT INTO ordered_products (order_id,price)VALUES
(2,958.3),(3,1568)";

// execute all requests
foreach ($insert_reqs as $key => $req) {
    echo 'Insert test datas into ' . $key . ' table<br>';
    $result = mysqli_query($conn, $req);
    echo mysqli_error($conn);
}
