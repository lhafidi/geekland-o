<?php
include('connexion.php');
mysqli_query($conn, 'set names "utf8";');

// suppresion detabke si déja exister 
$drop_req = "DROP TABLE IF EXISTS users, cart, cities, orders, ordered_products, products, product_categories, opinions;";
$result = mysqli_query($conn, $drop_req);

// create empty requests array
$table_reqs = [];

// users 
$table_reqs['users'] = 'CREATE TABLE IF NOT EXISTS users (id INT AUTO_INCREMENT PRIMARY KEY UNIQUE,name VARCHAR(191),lastname VARCHAR(191)
 ,email VARCHAR(191),password VARCHAR(191),adresse VARCHAR(191), 
 phone VARCHAR(191),activited BOOLEAN,city_id INT ,inscription_date DATE)';

// products
$table_reqs['cities'] = 'CREATE TABLE  IF NOT EXISTS cities (id INT AUTO_INCREMENT PRIMARY KEY UNIQUE,city VARCHAR(191),postal_code VARCHAR(191)
 ,country VARCHAR(191))';

// cart
$table_reqs['cart'] = 'CREATE TABLE  IF NOT EXISTS cart (id INT AUTO_INCREMENT PRIMARY KEY UNIQUE,total_cart FLOAT,product_count INT
 ,state_cart BOOLEAN,order_id INT)';

// orders
$table_reqs['orders'] = 'CREATE TABLE IF NOT EXISTS orders (id INT AUTO_INCREMENT PRIMARY KEY UNIQUE,user_id INT,total FLOAT,means_of_payment VARCHAR(191),
 delivery_method VARCHAR(191),products_count VARCHAR(191),delivery_date DATE)';

// ordered_products
$table_reqs['ordered_products'] = 'CREATE TABLE  IF NOT EXISTS ordered_products (id INT AUTO_INCREMENT PRIMARY KEY UNIQUE,order_id INT,price INT)';

// products
$table_reqs['products'] = 'CREATE TABLE IF NOT EXISTS  products (id INT AUTO_INCREMENT PRIMARY KEY UNIQUE,name VARCHAR(191),category_id SMALLINT,price FLOAT,description TEXT,
 model VARCHAR(191),brand VARCHAR(191),cut_price_percentage FLOAT,record_date DATE)';

// opinions
$table_reqs['opinions'] = 'CREATE TABLE IF NOT EXISTS  opinions (id INT AUTO_INCREMENT PRIMARY KEY UNIQUE,user_id INT,product_id INT,star_count TINYINT,date DATE,opinion TEXT)';

// product_categories
$table_reqs['product_categories'] = 'CREATE TABLE IF NOT EXISTS product_categories (id SMALLINT AUTO_INCREMENT PRIMARY KEY UNIQUE,category VARCHAR(191))';

// create table for each request in $table_reqs
foreach ($table_reqs as $key => $req) {
  echo 'Create '.$key.' table<br>';
  $res = $conn->query($req);
  echo mysqli_error($conn);
}
