<?php

include 'db/connexion.php';

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: *');

function encode_to_json($mixed)
{
    return json_encode($mixed, JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE);
}

$rubric = null;
$req_string = null;

// check if rubric argument exists and is not empty
if (isset($_GET['rubric']) && !empty($_GET['rubric'])) {
    $rubric = $_GET['rubric'];
}

// send errror if rubric is not good
if ($rubric === null) {
    die('{"error":"no rubric selected"}');
}

// 
switch ($rubric) {
    case 'carousel-news': // request 6 products for new products carousel
        $req_string = 'SELECT * FROM products ORDER BY record_date DESC LIMIT 6';
        break;

    case 'carousel-promotions': // request 6 products for products promotions carousel
        $req_string = 'SELECT * FROM products WHERE cut_price_percentage != 0 ORDER BY record_date DESC LIMIT 6';
        break;

    case 'all': // all products
        $req_string = 'SELECT * FROM products';
        break;

    case 'news': // new products
        $req_string = 'SELECT * FROM products ORDER BY record_date DESC';
        break;

    case 'promotions': // products in promotion
        $req_string = 'SELECT * FROM products WHERE cut_price_percentage != 0';
        break;

    case 'product': // one product
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $req_string = 'SELECT * FROM products WHERE id=' . $_GET['id'];
        }
        break;
    case 'reviewlist': // list reviews
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $req_string = '
            SELECT opinions.id, date, opinion, name, star_count
            FROM opinions
            LEFT JOIN users ON user_id=users.id
            WHERE product_id=' . $_GET['id'];
        }
        break;

    default:
        $req_string = null;
}


if ($req_string === null) {
    die('{"error":"rubric ' . $rubric . ' don\'t exists"}');
}

// query database with request string
$req = mysqli_query($conn, $req_string);

// fetch all row to datas array
$datas = mysqli_fetch_all($req, MYSQLI_ASSOC);

// if (mysqli_errno($conn)) {
//     die($req_string . ' ' . encode_to_json(mysqli_error($conn)));
// }

echo encode_to_json($datas);
